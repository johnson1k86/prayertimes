package de.iigc.prayertimes;

import de.iigc.prayertimes.springJavaFX.SpringFXMLLoader;
import de.iigc.prayertimes.springJavaFX.StageManager;
import java.io.IOException;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class AppJavaConfig {

  @Autowired @Lazy SpringFXMLLoader springFXMLLoader;

  @Bean
  @Lazy
  public StageManager stageManager(Stage stage) throws IOException {
    return new StageManager(springFXMLLoader, stage);
  }
}
