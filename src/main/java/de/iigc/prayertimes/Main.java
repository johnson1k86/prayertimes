package de.iigc.prayertimes;

import de.iigc.prayertimes.springJavaFX.FxmlView;
import de.iigc.prayertimes.springJavaFX.StageManager;
import javafx.application.Application;
import javafx.stage.Stage;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Main extends Application {

  private ConfigurableApplicationContext springContext;

  private StageManager stageManager;

  public static void main(String args[]) {

    Application.launch(args);
  }

  @Override
  public void init() {
    SpringApplicationBuilder builder = new SpringApplicationBuilder(Main.class);
    String[] args = getParameters().getRaw().stream().toArray(String[]::new);
    springContext = builder.run(args);
  }

  @Override
  public void start(Stage stage) throws Exception {

    stageManager = springContext.getBean(StageManager.class, stage);
    stageManager.switchScene(FxmlView.PRAYER);
    stage.setFullScreen(true);
  }

  @Override
  public void stop() {

    springContext.close();
  }
}
