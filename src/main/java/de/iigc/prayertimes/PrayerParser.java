package de.iigc.prayertimes;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.iigc.prayertimes.model.Prayers;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public class PrayerParser {

  private final Settings settings;
  private final Optional<File> jsonFile;

  public PrayerParser(Settings settings) {

    this.settings = settings;
    this.jsonFile = settings.getOptionalFilePrayer();
  }

  public Optional<Prayers> parse(int month, int day) throws IOException {

    if (jsonFile.isPresent()) {
      JsonNode times;

      ObjectMapper objectMapper = new ObjectMapper();
      times =
          objectMapper
              .readTree(jsonFile.get())
              .path("times")
              .path(Integer.toString(month))
              .path(Integer.toString(day));
      return Optional.ofNullable(objectMapper.treeToValue(times, Prayers.class));

    } else {

      return Optional.empty();
    }
  }
}
