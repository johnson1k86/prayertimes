package de.iigc.prayertimes;

import de.iigc.prayertimes.model.IqamaTime;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Optional;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Component;

@Component
public class Settings implements Serializable {

  private static final long serialVersionUID = 5L;

  private String masjidName;
  private String bankAccount;
  private File filePrayer;
  private boolean runningTextActive;
  private boolean hadithActive;
  private boolean wallpaperActive;
  private String runningText;
  private IqamaTime fajrIqama = IqamaTime.HALF_HOUR;
  private File fileHadith;
  private File folderImages;

  private transient Optional<File> optionalFilePrayer;
  private transient Optional<File> optionalFileHadith;
  private transient Optional<File> optionalFolderImages;

  Settings() {
    optionalFilePrayer = Optional.empty();
    optionalFileHadith = Optional.empty();
    optionalFolderImages = Optional.empty();
  }

  @PostConstruct
  public void init() {

    try {

      loadFromFile();

    } catch (IOException | ClassNotFoundException | NullPointerException e) {

    }

    if (filePrayer != null) setOptionalFilePrayer(Optional.of(filePrayer));
    if (fileHadith != null) setOptionalFileHadith(Optional.of(fileHadith));
    if (folderImages != null) setOptionalFolderImages(Optional.of(folderImages));
  }

  public String getMasjidName() {
    return masjidName;
  }

  public void setMasjidName(String masjidName) {
    this.masjidName = masjidName;
  }

  public String getBankAccount() {
    return bankAccount;
  }

  public void setBankAccount(String bankAccount) {
    this.bankAccount = bankAccount;
  }

  public File getFilePrayer() {
    return filePrayer;
  }

  public void setFilePrayer(File filePrayer) {
    this.filePrayer = filePrayer;
  }

  public Optional<File> getOptionalFilePrayer() {
    return optionalFilePrayer;
  }

  public void setOptionalFilePrayer(Optional<File> optionalFilePrayer) {
    this.optionalFilePrayer = optionalFilePrayer;
  }

  public boolean isRunningTextActive() {
    return runningTextActive;
  }

  public void setRunningTextActive(boolean runningTextActive) {
    this.runningTextActive = runningTextActive;
  }

  public String getRunningText() {
    return runningText;
  }

  public void setRunningText(String runningText) {
    this.runningText = runningText;
  }

  public IqamaTime getFajrIqama() {
    return fajrIqama;
  }

  public void setFajrIqama(IqamaTime fajrIqama) {
    this.fajrIqama = fajrIqama;
  }

  public File getFileHadith() {
    return fileHadith;
  }

  public void setFileHadith(File fileHadith) {
    this.fileHadith = fileHadith;
  }

  public File getFolderImages() {
    return folderImages;
  }

  public void setFolderImages(File folderImages) {
    this.folderImages = folderImages;
  }

  public Optional<File> getOptionalFileHadith() {
    return optionalFileHadith;
  }

  public void setOptionalFileHadith(Optional<File> optionalFileHadith) {
    this.optionalFileHadith = optionalFileHadith;
  }

  public Optional<File> getOptionalFolderImages() {
    return optionalFolderImages;
  }

  public void setOptionalFolderImages(Optional<File> optionalFolderImages) {
    this.optionalFolderImages = optionalFolderImages;
  }

  public boolean isHadithActive() {
    return hadithActive;
  }

  public void setHadithActive(boolean hadithActive) {
    this.hadithActive = hadithActive;
  }

  public boolean isWallpaperActive() {
    return wallpaperActive;
  }

  public void setWallpaperActive(boolean wallpaperActive) {
    this.wallpaperActive = wallpaperActive;
  }

  public void safeToFile() throws FileNotFoundException, IOException {

    FileOutputStream fout;
    ObjectOutputStream oout;
    fout = new FileOutputStream("./PrayerSettings.dat");
    oout = new ObjectOutputStream(fout);
    oout.writeObject(this);
    oout.close();
    fout.close();
  }

  public void loadFromFile() throws FileNotFoundException, IOException, ClassNotFoundException {

    FileInputStream fin;
    ObjectInputStream oin;
    fin = new FileInputStream("./PrayerSettings.dat");
    oin = new ObjectInputStream(fin);
    Settings tempSettings = (Settings) oin.readObject();
    setMasjidName(tempSettings.getMasjidName());
    setBankAccount(tempSettings.getBankAccount());
    setRunningText(tempSettings.getRunningText());
    setFajrIqama(tempSettings.getFajrIqama());
    setRunningTextActive(tempSettings.isRunningTextActive());
    setFilePrayer(tempSettings.getFilePrayer());
    setFileHadith(tempSettings.getFileHadith());
    setHadithActive(tempSettings.isHadithActive());
    setWallpaperActive(tempSettings.isWallpaperActive());
    setFolderImages(tempSettings.getFolderImages());
    oin.close();
    fin.close();
  }
}
