package de.iigc.prayertimes.fxControllers;

import de.iigc.prayertimes.springJavaFX.FxmlView;
import de.iigc.prayertimes.springJavaFX.StageManager;
import java.net.URL;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class CountdownSceneController implements Initializable {

  @Autowired @Lazy private StageManager stageManager;

  @FXML private Label labelCountdown;

  private Timeline timeline;

  private SimpleStringProperty stringCountdown;

  private long minutes;
  private Duration countdownTime;
  private DateTimeFormatter formatter;

  @Override
  public void initialize(URL url, ResourceBundle rb) {

    minutes = 10L;
    countdownTime = Duration.ofMinutes(minutes);
    formatter = DateTimeFormatter.ofPattern("mm:ss");

    stringCountdown = new SimpleStringProperty();
    labelCountdown.textProperty().bind(stringCountdown);

    timeline =
        new Timeline(
            new KeyFrame(
                javafx.util.Duration.seconds(1),
                (t) -> {
                  countdownTime = countdownTime.minusSeconds(1L);
                  setStringCountdown(LocalTime.MIDNIGHT.plus(countdownTime).format(formatter));
                  if (countdownTime.isZero()) {
                    timeline.stop();
                    stageManager.switchScene(FxmlView.PRAYER);
                  }
                }));
    timeline.setCycleCount(Timeline.INDEFINITE);
    timeline.playFromStart();
  }

  public SimpleStringProperty stringCountdownProperty() {

    return stringCountdown;
  }

  public String getStringCountdown() {
    return stringCountdown.get();
  }

  public void setStringCountdown(String countdown) {

    stringCountdown.set(countdown);
  }
}
