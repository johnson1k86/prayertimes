/*
 * Copyright (C) 2020 georg
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.iigc.prayertimes.fxControllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.iigc.prayertimes.Settings;
import de.iigc.prayertimes.model.Hadith;
import de.iigc.prayertimes.springJavaFX.FxmlView;
import de.iigc.prayertimes.springJavaFX.StageManager;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class HadithSceneController implements Initializable {

  @Autowired @Lazy private StageManager stageManager;

  ObjectMapper om;

  Settings settings;

  @FXML private Label labelArabic;

  @FXML private Label labelLatin;

  Timeline timeline;

  long minutes = 1L;
  Duration durationMinutes;

  Optional<File> fileHadith;

  public HadithSceneController(Settings settings) {

    this.settings = settings;
    try {
      fileHadith = Optional.of(settings.getFileHadith());
    } catch (NullPointerException e) {
      fileHadith = Optional.empty();
    }
    om = new ObjectMapper();
    durationMinutes = Duration.ofMinutes(minutes);
  }

  @Override
  public void initialize(URL url, ResourceBundle rb) {

    ArrayList<Hadith> hl = new ArrayList<>();
    if (fileHadith.isPresent()) {
      try {
        hl.addAll(Arrays.asList(om.readValue(fileHadith.get(), Hadith[].class)));
        int sizeHadith = hl.size() - 1;
        Random random = new Random();
        int numberHadith = random.nextInt(sizeHadith);
        labelArabic.setText(hl.get(numberHadith).getArabic());
        labelLatin.setText(hl.get(numberHadith).getLatin());
        timeline =
            new Timeline(
                new KeyFrame(
                    javafx.util.Duration.seconds(1),
                    (t) -> {
                      durationMinutes = durationMinutes.minusSeconds(1L);
                      if (durationMinutes.isZero()) {
                        stageManager.switchScene(FxmlView.PRAYER);
                      }
                    }));
        timeline.playFromStart();

      } catch (IOException ex) {
        Logger.getLogger(HadithSceneController.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
}
