package de.iigc.prayertimes.fxControllers;

import de.iigc.prayertimes.Settings;
import de.iigc.prayertimes.model.Prayers;
import de.iigc.prayertimes.springJavaFX.FxmlView;
import de.iigc.prayertimes.springJavaFX.StageManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Interpolator;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.CacheHint;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class PrayerSceneController implements Initializable {

  private final SimpleStringProperty stringHijri;
  private final SimpleStringProperty stringGreg;
  private final SimpleStringProperty stringTime;
  private final SimpleStringProperty stringFajr;
  private final SimpleStringProperty stringShuruk;
  private final SimpleStringProperty stringDhuhur;
  private final SimpleStringProperty stringAsr;
  private final SimpleStringProperty stringMaghrib;
  private final SimpleStringProperty stringIsha;
  private TranslateTransition tt;

  @FXML private StackPane paneBackground;

  @FXML private Label labelMasjidName;

  @FXML private Label labelHijriDate;

  @FXML private Label labelGregDate;

  @FXML private Label labelTime;

  @FXML private Label labelFajr;

  @FXML private Label labelShuruk;

  @FXML private Label labelDhuhur;

  @FXML private Label labelAsr;

  @FXML private Label labelMaghrib;

  @FXML private Label labelIsha;

  @FXML private Label labelAccount;

  @FXML private Button buttonSettings;

  @FXML private Text runningText;

  private final Settings settings;

  Background background;

  @Autowired @Lazy private StageManager stageManager;

  @Autowired
  PrayerSceneController(Settings settings) {

    this.settings = settings;
    stringHijri = new SimpleStringProperty();
    stringGreg = new SimpleStringProperty();
    stringTime = new SimpleStringProperty();
    stringFajr = new SimpleStringProperty();
    stringShuruk = new SimpleStringProperty();
    stringDhuhur = new SimpleStringProperty();
    stringAsr = new SimpleStringProperty();
    stringMaghrib = new SimpleStringProperty();
    stringIsha = new SimpleStringProperty();
  }

  @Override
  public void initialize(URL url, ResourceBundle rb) {

    labelMasjidName.setText(settings.getMasjidName());
    labelGregDate.textProperty().bind(stringGreg);
    labelHijriDate.textProperty().bind(stringHijri);
    labelTime.textProperty().bind(stringTime);
    labelFajr.textProperty().bind(stringFajr);
    labelShuruk.textProperty().bind(stringShuruk);
    labelDhuhur.textProperty().bind(stringDhuhur);
    labelAsr.textProperty().bind(stringAsr);
    labelMaghrib.textProperty().bind(stringMaghrib);
    labelIsha.textProperty().bind(stringIsha);
    labelAccount.setText(settings.getBankAccount());
    displayImage();
    runVerticalText();
  }

  public void updateTime(String time) {
    Platform.runLater(
        () -> {
          setStringTime(time);
        });
  }

  public void updateGregDate(String date) {
    Platform.runLater(
        () -> {
          setStringGreg(date);
        });
  }

  public void updateHijriDate(String date) {
    Platform.runLater(
        () -> {
          setStringHijri(date);
        });
  }

  public void updatePrayers(Prayers prayers) {
    Platform.runLater(
        () -> {
          setStringFajr(prayers.getFajr());
          setStringShuruk(prayers.getShuruk());
          setStringDhuhur(prayers.getDhuhur());
          setStringAsr(prayers.getAsr());
          setStringMaghrib(prayers.getMaghrib());
          setStringIsha(prayers.getIsha());
        });
  }

  public SimpleStringProperty stringHijriProperty() {
    return stringHijri;
  }

  public String getStringHijri() {

    return stringHijri.get();
  }

  public void setStringHijri(String date) {

    stringHijri.set(date);
  }

  public SimpleStringProperty stringGregProperty() {
    return stringHijri;
  }

  public String getStringGreg() {

    return stringGreg.get();
  }

  public void setStringGreg(String date) {

    stringGreg.set(date);
  }

  public SimpleStringProperty stringTimeProperty() {
    return stringTime;
  }

  public String getStringTime() {

    return stringTime.get();
  }

  public void setStringTime(String time) {

    stringTime.set(time);
  }

  public SimpleStringProperty stringFajrProperty() {
    return stringFajr;
  }

  public SimpleStringProperty stringShurukProperty() {
    return stringShuruk;
  }

  public SimpleStringProperty stringDhuhurProperty() {
    return stringDhuhur;
  }

  public SimpleStringProperty stringAsrProperty() {
    return stringAsr;
  }

  public SimpleStringProperty stringMaghribProperty() {
    return stringMaghrib;
  }

  public SimpleStringProperty stringIshaProperty() {
    return stringIsha;
  }

  public String getStringFajr() {
    return stringFajr.get();
  }

  public String getStringShuruk() {
    return stringShuruk.get();
  }

  public String getStringDhuhur() {
    return stringDhuhur.get();
  }

  public String getStringAsr() {
    return stringAsr.get();
  }

  public String getStringMaghrib() {
    return stringMaghrib.get();
  }

  public String getStringIsha() {
    return stringIsha.get();
  }

  public void setStringFajr(String fajr) {

    stringFajr.set(fajr);
  }

  public void setStringShuruk(String shuruk) {

    stringShuruk.set(shuruk);
  }

  public void setStringDhuhur(String dhuhur) {

    stringDhuhur.set(dhuhur);
  }

  public void setStringAsr(String asr) {

    stringAsr.set(asr);
  }

  public void setStringMaghrib(String maghrib) {

    stringMaghrib.set(maghrib);
  }

  public void setStringIsha(String isha) {

    stringIsha.set(isha);
  }

  @FXML
  void openSettings(ActionEvent event) {

    stageManager.switchScene(FxmlView.SETTINGS);
  }

  @FXML
  void unhideSettings(MouseEvent event) throws InterruptedException {

    if (buttonSettings.isDisabled() && !buttonSettings.isVisible()) {

      buttonSettings.setDisable(false);
      buttonSettings.setVisible(true);

    } else {

      buttonSettings.setDisable(true);
      buttonSettings.setVisible(false);
    }
  }

  private void runVerticalText() {

    if (settings.isRunningTextActive()) {
      Platform.runLater(
          () -> {
            runningText.setText(settings.getRunningText());
            runningText.setCache(true);
            runningText.setCacheHint(CacheHint.SPEED);
            tt = new TranslateTransition(Duration.seconds(30), runningText);
            tt.setFromX(
                paneBackground.getScene().getWindow().getWidth()
                    - runningText.getLayoutBounds().getWidth());
            tt.setToX(0);
            tt.setCycleCount(TranslateTransition.INDEFINITE);
            tt.setInterpolator(Interpolator.LINEAR);
            tt.play();
          });
    } else {
    }
  }

  private void displayImage() {
    if (settings.isWallpaperActive()
        && !settings.getOptionalFolderImages().isEmpty()
        && settings.getOptionalFolderImages().get().exists()) {
      ArrayList<File> images = new ArrayList<>();
      images.addAll(Arrays.asList(settings.getFolderImages().listFiles()));
      int randomNum = ThreadLocalRandom.current().nextInt(0, images.size());
      FileInputStream fin = null;
      try {
        fin = new FileInputStream(images.get(randomNum));
      } catch (FileNotFoundException ex) {
        Logger.getLogger(PrayerSceneController.class.getName())
            .log(Level.SEVERE, "Konnte Hintergrundbild nicht öffnen", ex);
      }
      Image image = new Image(fin);
      BackgroundImage backgroundimage =
          new BackgroundImage(
              image,
              BackgroundRepeat.SPACE,
              BackgroundRepeat.SPACE,
              BackgroundPosition.CENTER,
              new BackgroundSize(100.0, 100.0, true, true, true, true));
      background = new Background(backgroundimage);
      paneBackground.setBackground(background);
    }
  }
}
