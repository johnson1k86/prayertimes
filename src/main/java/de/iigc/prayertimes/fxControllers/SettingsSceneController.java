package de.iigc.prayertimes.fxControllers;

import de.iigc.prayertimes.Settings;
import de.iigc.prayertimes.model.IqamaTime;
import de.iigc.prayertimes.springJavaFX.FxmlView;
import de.iigc.prayertimes.springJavaFX.StageManager;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class SettingsSceneController implements Initializable {

  @FXML private TextField textMasjidName;

  @FXML private TextArea textAreaBankAccount;

  @FXML private Button buttonCancel;

  @FXML private Button buttonOk;

  @FXML private TextField textRunningText;

  @FXML private CheckBox checkRunningText;

  @FXML private ChoiceBox<IqamaTime> choiceFajrIqama;

  @FXML private Button buttonSelectPrayerJson;

  @FXML private Button buttonSelectHadithJson;

  @FXML private Button buttonSelectImageFolder;

  @FXML private CheckBox checkHadith;

  @FXML private CheckBox checkWallpaper;

  private File filePrayer;
  private File fileHadith;
  private File folderImages;

  private final Settings settings;

  @Autowired @Lazy StageManager stageManager;

  public SettingsSceneController(Settings settings) {

    choiceFajrIqama = new ChoiceBox<>();
    this.settings = settings;
  }

  @Override
  public void initialize(URL url, ResourceBundle rb) {

    choiceFajrIqama.getItems().setAll(IqamaTime.values());
    textMasjidName.setText(settings.getMasjidName());
    textAreaBankAccount.setText(settings.getBankAccount());
    textRunningText.setText(settings.getRunningText());
    choiceFajrIqama.setValue(settings.getFajrIqama());
    checkRunningText.setSelected(settings.isRunningTextActive());
    filePrayer = settings.getFilePrayer();
    fileHadith = settings.getFileHadith();
    folderImages = settings.getFolderImages();
    checkHadith.setSelected(settings.isHadithActive());
    checkWallpaper.setSelected(settings.isWallpaperActive());
  }

  @FXML
  private void openFileChooserPrayer(ActionEvent event) {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Gebetszeitendatei wählen");
    fileChooser
        .getExtensionFilters()
        .add(new FileChooser.ExtensionFilter("JSON Dateien", "*.json"));
    filePrayer = fileChooser.showOpenDialog(new Stage());
  }

  @FXML
  private void openFileChooserHadith(ActionEvent event) {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Hadithdatei wählen");
    fileChooser
        .getExtensionFilters()
        .add(new FileChooser.ExtensionFilter("JSON Dateien", "*.json"));
    fileHadith = fileChooser.showOpenDialog(new Stage());
  }

  @FXML
  private void openDirectoryChooserImages(ActionEvent event) {
    DirectoryChooser directoryChooser = new DirectoryChooser();
    directoryChooser.setTitle("Ordner mit Bildern wählen");
    folderImages = directoryChooser.showDialog(new Stage());
  }

  @FXML
  private void saveSettings(ActionEvent event) {
    settings.setMasjidName(textMasjidName.getText());
    settings.setBankAccount(textAreaBankAccount.getText());
    settings.setFilePrayer(filePrayer);
    settings.setRunningText(textRunningText.getText());
    settings.setRunningTextActive(checkRunningText.isSelected());
    settings.setFajrIqama(choiceFajrIqama.getValue());
    settings.setFileHadith(fileHadith);
    settings.setFolderImages(folderImages);
    settings.setHadithActive(checkHadith.isSelected());
    settings.setWallpaperActive(checkWallpaper.isSelected());
    try {
      settings.safeToFile();
    } catch (IOException ex) {
      Logger.getLogger(HadithSceneController.class.getName()).log(Level.SEVERE, null, ex);
    }
    stageManager.switchScene(FxmlView.PRAYER);
  }

  @FXML
  void abortSettings(ActionEvent event) {

    stageManager.switchScene(FxmlView.PRAYER);
  }
}
