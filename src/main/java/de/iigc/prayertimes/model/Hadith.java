/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.iigc.prayertimes.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/** @author georg */
public class Hadith {

  private final SimpleStringProperty arabic;
  private final SimpleStringProperty latin;

  public Hadith() {

    arabic = new SimpleStringProperty(new String());
    latin = new SimpleStringProperty(new String());
  }

  public String getArabic() {
    return arabic.get();
  }

  public void setArabic(String arabic) {
    this.arabic.set(arabic);
  }

  public String getLatin() {
    return latin.get();
  }

  public void setLatin(String latin) {
    this.latin.set(latin);
  }

  public StringProperty arabicProperty() {
    return arabic;
  }

  public StringProperty latinProperty() {
    return latin;
  }
}
