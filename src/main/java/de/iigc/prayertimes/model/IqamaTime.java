package de.iigc.prayertimes.model;

public enum IqamaTime {
  NO_DELAY("+00:00", 0, 0),

  QUARTER_HOUR("+00:15", 0, 15),

  HALF_HOUR("+00:30", 0, 30),

  THREEQUARTER_HOUR("+00:45", 0, 45),

  HOUR("+01:00", 1, 0),

  ONE_HOUR_AND_A_QUARTER_HOUR("+01:15", 1, 15),

  ONE_HOUR_AND_A_HALF_HOUR("+01:30", 1, 30),

  ONE_HOUR_AND_THREEQUARTER_HOUR("+01:45", 1, 45),

  TWO_HOURS("+02:00", 2, 0);

  private final String label;
  private final int intervalHour;
  private final int intervalMinute;

  IqamaTime(String label, int intervalHour, int intervalMinute) {
    this.label = label;
    this.intervalHour = intervalHour;
    this.intervalMinute = intervalMinute;
  }

  public int getIntervalHour() {
    return intervalHour;
  }

  public int getIntervalMinute() {
    return intervalMinute;
  }

  @Override
  public String toString() {
    return label;
  }
}
