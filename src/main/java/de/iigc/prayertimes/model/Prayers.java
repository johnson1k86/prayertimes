package de.iigc.prayertimes.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.boot.json.JacksonJsonParser;

public class Prayers {

  private String fajr;
  private String shuruk;
  private String dhuhur;
  private String asr;
  private String maghrib;
  private String isha;

  public String getFajr() {
    return fajr;
  }

  @JsonProperty("p1")
  public void setFadjr(JsonNode fadjr) {
    this.fajr = new JacksonJsonParser().parseMap(fadjr.toString()).get("t").toString();
  }

  public String getShuruk() {
    return shuruk;
  }

  @JsonProperty("p2")
  public void setShuruk(JsonNode shuruk) {
    this.shuruk = new JacksonJsonParser().parseMap(shuruk.toString()).get("t").toString();
  }

  public String getDhuhur() {
    return dhuhur;
  }

  @JsonProperty("p3")
  public void setDhuhur(JsonNode dhuhur) {
    this.dhuhur = new JacksonJsonParser().parseMap(dhuhur.toString()).get("t").toString();
  }

  public String getAsr() {
    return asr;
  }

  @JsonProperty("p4")
  public void setAsr(JsonNode asr) {
    this.asr = new JacksonJsonParser().parseMap(asr.toString()).get("t").toString();
  }

  public String getMaghrib() {
    return maghrib;
  }

  @JsonProperty("p5")
  public void setMaghrib(JsonNode maghrib) {
    this.maghrib = new JacksonJsonParser().parseMap(maghrib.toString()).get("t").toString();
  }

  public String getIsha() {
    return isha;
  }

  @JsonProperty("p6")
  public void setIsha(JsonNode isha) {
    this.isha = new JacksonJsonParser().parseMap(isha.toString()).get("t").toString();
  }
}
