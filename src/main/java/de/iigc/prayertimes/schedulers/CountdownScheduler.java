package de.iigc.prayertimes.schedulers;

import de.iigc.prayertimes.PrayerParser;
import de.iigc.prayertimes.Settings;
import de.iigc.prayertimes.model.Prayers;
import de.iigc.prayertimes.springJavaFX.FxmlView;
import de.iigc.prayertimes.springJavaFX.StageManager;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

@Configuration
@EnableScheduling
public class CountdownScheduler implements SchedulingConfigurer {

  @Autowired @Lazy private StageManager stageManager;

  private final PrayerParser prayerParser;

  private final Settings settings;

  public CountdownScheduler(PrayerParser prayerParser, Settings settings) {

    this.prayerParser = prayerParser;
    this.settings = settings;
  }

  public Date computeNextPrayerCron() {

    Date iqama = null;

    try {
      int month = LocalDate.now().atStartOfDay().getMonthValue();
      int day = LocalDate.now().atStartOfDay().getDayOfMonth();
      int monthTomorrow = LocalDate.now().atStartOfDay().plusDays(1L).getMonthValue();
      int dayTomorrow = LocalDate.now().atStartOfDay().plusDays(1L).getDayOfMonth();

      LocalTime now = LocalTime.now();

      Optional<Prayers> prayers = prayerParser.parse(month, day);
      Optional<Prayers> prayersOfTomorrow = prayerParser.parse(monthTomorrow, dayTomorrow);

      if (prayers.isPresent()) {
        LocalTime fajr = LocalTime.parse(prayers.get().getFajr());
        LocalTime dhuhur = LocalTime.parse(prayers.get().getDhuhur());
        LocalTime asr = LocalTime.parse(prayers.get().getAsr());
        LocalTime maghrib = LocalTime.parse(prayers.get().getMaghrib());
        LocalTime isha = LocalTime.parse(prayers.get().getIsha());
        LocalTime fajrOfTomorrow = LocalTime.parse(prayersOfTomorrow.get().getFajr());

        if (now.isBefore(fajr)) {
          iqama =
              new Date(
                  fajr.atDate(LocalDate.now())
                      .atZone(ZoneId.systemDefault())
                      .toInstant()
                      .toEpochMilli());
        } else if (now.isBefore(dhuhur)) {
          iqama =
              new Date(
                  dhuhur
                      .atDate(LocalDate.now())
                      .atZone(ZoneId.systemDefault())
                      .toInstant()
                      .toEpochMilli());

        } else if (now.isBefore(asr)) {
          iqama =
              new Date(
                  asr.atDate(LocalDate.now())
                      .atZone(ZoneId.systemDefault())
                      .toInstant()
                      .toEpochMilli());

        } else if (now.isBefore(maghrib)) {
          iqama =
              new Date(
                  maghrib
                      .atDate(LocalDate.now())
                      .atZone(ZoneId.systemDefault())
                      .toInstant()
                      .toEpochMilli());

        } else if (now.isBefore(isha)) {
          iqama =
              new Date(
                  isha.atDate(LocalDate.now())
                      .atZone(ZoneId.systemDefault())
                      .toInstant()
                      .toEpochMilli());

        } else {
          try {
            iqama =
                new Date(
                    fajrOfTomorrow
                        .atDate(LocalDate.now())
                        .plusDays(1L)
                        .plusHours(settings.getFajrIqama().getIntervalHour())
                        .plusMinutes(settings.getFajrIqama().getIntervalMinute() - 10)
                        .atZone(ZoneId.systemDefault())
                        .toInstant()
                        .toEpochMilli());
          } catch (NullPointerException n) {
            Logger.getLogger(CountdownScheduler.class.getName())
                .log(Level.SEVERE, "Fehler Berechnung Fajr-Iqama", n);
          }
        }
      }

    } catch (IOException ex) {
      Logger.getLogger(CountdownScheduler.class.getName())
          .log(Level.SEVERE, "Fehler Berechnung Countdown-Timer", ex);
    }

    return iqama;
  }

  @Override
  public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {

    taskRegistrar.addTriggerTask(
        () -> {
          stageManager.switchScene(FxmlView.COUNTDOWN);
        },
        (TriggerContext triggerContext) -> {
          Date nextExec = computeNextPrayerCron();
          return nextExec;
        });
  }
}
