package de.iigc.prayertimes.schedulers;

import de.iigc.prayertimes.PrayerParser;
import de.iigc.prayertimes.fxControllers.PrayerSceneController;
import de.iigc.prayertimes.model.Prayers;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class GregCalendar {

  private LocalTime currentTime;
  private LocalDate currentDate;
  private final PrayerSceneController prayerSceneController;
  private final PrayerParser prayerParser;
  private final DateTimeFormatter formatterTime;
  private final DateTimeFormatter formatterDate;

  GregCalendar(PrayerSceneController prayerSceneController, PrayerParser prayerParser) {

    this.prayerSceneController = prayerSceneController;
    this.prayerParser = prayerParser;
    this.formatterTime = DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM);
    this.formatterDate = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);
  }

  @PostConstruct
  public void init() throws InterruptedException, IOException {

    newDay();
  }

  @Scheduled(cron = "0 0 0 * * ?")
  private void newDay() throws IOException {

    setCurrentDate(LocalDate.now());
    prayerSceneController.updateGregDate(getCurrentDate().format(formatterDate));
    updatePrayers();
  }

  @Scheduled(fixedRate = 1000)
  private void tick() {

    setCurrentTime(LocalTime.now());
    prayerSceneController.updateTime(getCurrentTime().format(formatterTime));
  }

  public LocalTime getCurrentTime() {
    return currentTime;
  }

  public void setCurrentTime(LocalTime currentTime) {
    this.currentTime = currentTime;
  }

  public LocalDate getCurrentDate() {
    return currentDate;
  }

  public void setCurrentDate(LocalDate currentDate) {
    this.currentDate = currentDate;
  }

  public void updatePrayers() {

    int month = getCurrentDate().getMonthValue();
    int day = getCurrentDate().getDayOfMonth();
    Optional<Prayers> prayers;
    try {
      prayers = prayerParser.parse(month, day);

      if (prayers.isPresent()) {
        prayerSceneController.updatePrayers(prayers.get());
      } else {
      }
    } catch (IOException e) {

      Logger LOG = Logger.getLogger(GregCalendar.class.getName());
      LOG.log(Level.SEVERE, "Fehler beim Update von Gebeten", e);
    }
  }
}
