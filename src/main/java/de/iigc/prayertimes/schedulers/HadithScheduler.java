/*
 * Copyright (C) 2020 georg
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.iigc.prayertimes.schedulers;

import de.iigc.prayertimes.PrayerParser;
import de.iigc.prayertimes.Settings;
import de.iigc.prayertimes.model.Prayers;
import de.iigc.prayertimes.springJavaFX.FxmlView;
import de.iigc.prayertimes.springJavaFX.StageManager;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/** @author georg */
@Configuration
@EnableScheduling
public class HadithScheduler implements SchedulingConfigurer {

  private final Settings settings;
  private final PrayerParser prayerParser;
  Date hadith;
  long minutes;

  @Autowired @Lazy StageManager stageManager;

  public HadithScheduler(PrayerParser prayerParser, Settings settings) {

    this.prayerParser = prayerParser;
    this.settings = settings;
    minutes = 15L;
  }

  public Date computeNextHadithCron() {
    Date iqama = null;

    try {
      int month = LocalDate.now().atStartOfDay().getMonthValue();
      int day = LocalDate.now().atStartOfDay().getDayOfMonth();
      int monthTomorrow = LocalDate.now().atStartOfDay().plusDays(1L).getMonthValue();
      int dayTomorrow = LocalDate.now().atStartOfDay().plusDays(1L).getDayOfMonth();

      LocalTime now = LocalTime.now();
      Optional<Prayers> prayers = prayerParser.parse(month, day);
      Optional<Prayers> prayersOfTomorrow = prayerParser.parse(monthTomorrow, dayTomorrow);

      if (prayers.isPresent()) {
        LocalTime dhuhur = LocalTime.parse(prayers.get().getDhuhur());
        LocalTime asr = LocalTime.parse(prayers.get().getAsr());
        LocalTime maghrib = LocalTime.parse(prayers.get().getMaghrib());
        LocalTime isha = LocalTime.parse(prayers.get().getIsha());
        LocalTime fajrOfTomorrow = LocalTime.parse(prayersOfTomorrow.get().getFajr());

        if (now.isBefore(dhuhur)) {
          hadith =
              new Date(
                  dhuhur
                      .atDate(LocalDate.now())
                      .atZone(ZoneId.systemDefault())
                      .plusMinutes(minutes)
                      .toInstant()
                      .toEpochMilli());

        } else if (now.isBefore(asr)) {
          hadith =
              new Date(
                  asr.atDate(LocalDate.now())
                      .atZone(ZoneId.systemDefault())
                      .plusMinutes(minutes)
                      .toInstant()
                      .toEpochMilli());

        } else if (now.isBefore(maghrib)) {
          hadith =
              new Date(
                  maghrib
                      .atDate(LocalDate.now())
                      .atZone(ZoneId.systemDefault())
                      .plusMinutes(minutes)
                      .toInstant()
                      .toEpochMilli());

        } else if (now.isBefore(isha)) {
          hadith =
              new Date(
                  isha.atDate(LocalDate.now())
                      .atZone(ZoneId.systemDefault())
                      .plusMinutes(minutes)
                      .toInstant()
                      .toEpochMilli());

        } else {
          try {
            hadith =
                new Date(
                    fajrOfTomorrow
                        .atDate(LocalDate.now())
                        .plusDays(1L)
                        .plusHours(settings.getFajrIqama().getIntervalHour())
                        .plusMinutes(settings.getFajrIqama().getIntervalMinute() - 10)
                        .atZone(ZoneId.systemDefault())
                        .plusMinutes(minutes)
                        .toInstant()
                        .toEpochMilli());
          } catch (NullPointerException n) {

            Logger.getLogger(HadithScheduler.class.getName())
                .log(Level.SEVERE, "Fehler Berechnung Fajr-Hadithzeit");
          }
        }
      }

    } catch (IOException ex) {
      Logger.getLogger(HadithScheduler.class.getName()).log(Level.SEVERE, "Fehler Hadith Timer");
    }

    return hadith;
  }

  @Override
  public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {

    if (settings.isHadithActive()) {

      taskRegistrar.addTriggerTask(
          () -> {
            stageManager.switchScene(FxmlView.HADITH);
          },
          (TriggerContext triggerContext) -> {
            Date nextExec = computeNextHadithCron();
            return nextExec;
          });
    }
  }
}
