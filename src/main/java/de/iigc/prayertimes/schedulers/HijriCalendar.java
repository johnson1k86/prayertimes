package de.iigc.prayertimes.schedulers;

import de.iigc.prayertimes.fxControllers.PrayerSceneController;
import java.time.chrono.HijrahDate;
import java.time.format.DateTimeFormatter;
import javax.annotation.PostConstruct;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class HijriCalendar {

  private HijrahDate hijriDate;
  private final DateTimeFormatter formatterDate;
  private final PrayerSceneController prayerSceneController;

  public HijriCalendar(PrayerSceneController prayerSceneController) {
    this.prayerSceneController = prayerSceneController;
    this.formatterDate = DateTimeFormatter.ofPattern("dd.LL.yyyy");
  }

  @PostConstruct
  public void init() {

    newDay();
  }

  @Scheduled(cron = "0 0 0 * * ?")
  private void newDay() {
    setHijriDate(HijrahDate.now());
    prayerSceneController.updateHijriDate(getHijriDate().format(formatterDate));
  }

  public HijrahDate getHijriDate() {
    return hijriDate;
  }

  public void setHijriDate(HijrahDate hijriDate) {
    this.hijriDate = hijriDate;
  }
}
