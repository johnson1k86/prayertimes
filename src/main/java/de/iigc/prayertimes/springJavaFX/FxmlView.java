package de.iigc.prayertimes.springJavaFX;

public enum FxmlView {
  PRAYER {

    @Override
    String getFxmlFile() {
      return "/fxml/prayerscene.fxml";
    }
  },
  SETTINGS {

    @Override
    String getFxmlFile() {
      return "/fxml/settingsscene.fxml";
    }
  },
  COUNTDOWN {

    @Override
    String getFxmlFile() {
      return "/fxml/countdownscene.fxml";
    }
  },
  HADITH {

    @Override
    String getFxmlFile() {
      return "/fxml/hadithscene.fxml";
    }
  };

  abstract String getFxmlFile();
}
