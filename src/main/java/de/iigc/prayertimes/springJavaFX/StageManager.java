/*
Stage Manager and SpringFXMLLoader from mvp
https://www.facebook.com/mvpjava
 */
package de.iigc.prayertimes.springJavaFX;

import java.io.IOException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/** Manages switching Scenes on the Primary Stage */
public class StageManager {

  private final Stage primaryStage;
  private final SpringFXMLLoader springFXMLLoader;

  public StageManager(SpringFXMLLoader springFXMLLoader, Stage stage) {
    this.springFXMLLoader = springFXMLLoader;
    this.primaryStage = stage;
  }

  public void switchScene(final FxmlView view) {
    Parent viewRootNodeHierarchy = loadViewNodeHierarchy(view.getFxmlFile());
    show(viewRootNodeHierarchy);
  }

  private void show(final Parent rootnode) {
    Scene scene = prepareScene(rootnode);
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  private Scene prepareScene(Parent rootnode) {
    Scene scene = primaryStage.getScene();

    if (scene == null) {
      scene = new Scene(rootnode);
    }
    scene.setRoot(rootnode);
    return scene;
  }

  /**
   * Loads the object hierarchy from a FXML document and returns to root node of that hierarchy.
   *
   * @return Parent root node of the FXML document hierarchy
   */
  private Parent loadViewNodeHierarchy(String fxmlFilePath) {
    Parent rootNode = null;
    try {
      rootNode = springFXMLLoader.load(fxmlFilePath);
      Objects.requireNonNull(rootNode, "A Root FXML node must not be null");
    } catch (IOException exception) {
      logAndExit("Unable to load FXML view" + fxmlFilePath);
    }
    return rootNode;
  }

  private void logAndExit(String errorMsg) {
    Logger.getLogger(StageManager.class.getName()).log(Level.SEVERE, errorMsg);
    Platform.exit();
    Platform.exit();
  }
}
