/*
 * Copyright (C) 2020 Pivotal Software, Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module PrayerTimes {
  requires java.base;
  requires java.logging;
  requires spring.boot.starter.quartz;
  requires spring.boot.starter;
  requires spring.boot;
  requires spring.boot.autoconfigure;
  requires spring.boot.starter.logging;
  requires java.annotation;
  requires spring.core;
  requires spring.jcl;
  requires org.yaml.snakeyaml;
  requires spring.context.support;
  requires spring.beans;
  requires spring.context;
  requires spring.aop;
  requires spring.expression;
  requires spring.tx;
  requires quartz;
  requires mchange.commons.java;
  requires com.fasterxml.jackson.databind;
  requires com.fasterxml.jackson.annotation;
  requires com.fasterxml.jackson.core;
  requires javafx.base;
  requires javafx.controls;
  requires javafx.fxml;
  requires javafx.graphics;

  exports de.iigc.prayertimes;
}
